package strategy.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Kacper Urbaniec
 * @version 18.02.2019
 */
public class AdditionTest {
    private Calculator<Number> calculator;

    @Before
    public void setUp() throws Exception {
        this.calculator = new Calculator<Number>();
        this.calculator.setCalculations(new Addition());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void additionDouble() {
        Double firstValue = 42.0;
        Double secondValue = 23.0;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(2.0);
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[44.0, 25.0]", result.toString());
    }

    @Test
    public void additionInteger() {
        Integer firstValue = 42;
        Integer secondValue = 23;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(2);
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[44.0, 25.0]", result.toString());
    }

    @Test
    public void additionBigDecimal() {
        BigDecimal firstValue = BigDecimal.valueOf(25000002500000.25);
        BigDecimal secondValue = BigDecimal.valueOf(75000007500000.75);
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(BigDecimal.valueOf(2500000.25));
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[25000005000000.50, 75000010000001.00]", result.toString());
    }

    @Test
    public void additionBigInteger() {
        BigInteger firstValue = new BigInteger("25000002500000");
        BigInteger secondValue = new BigInteger("75000007500000");
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(new BigInteger("2500000"));
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[25000005000000, 75000010000000]", result.toString());
    }

    @Test
    public void additionBigIntegerInteger() {
        BigInteger firstValue = new BigInteger("25000002500000");
        BigInteger secondValue = new BigInteger("75000007500000");
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(2500000);
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[25000005000000.0, 75000010000000.0]", result.toString());
    }
}