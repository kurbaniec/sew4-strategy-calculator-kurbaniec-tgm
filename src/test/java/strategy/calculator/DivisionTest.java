package strategy.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Kacper Urbaniec
 * @version 18.02.2019
 */
public class DivisionTest {
    private Calculator<Number> calculator;

    @Before
    public void setUp() throws Exception {
        this.calculator = new Calculator<Number>();
        this.calculator.setCalculations(new Division());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void divisionDouble() {
        Double firstValue = 42.0;
        Double secondValue = 23.0;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(2.0);
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[21.0, 11.5]", result.toString());
    }

    @Test
    public void divisionInteger() {
        Integer firstValue = 42;
        Integer secondValue = 23;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(2);
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[21.0, 11.5]", result.toString());
    }

    @Test
    public void divisionBigDecimal() {
        BigDecimal firstValue = BigDecimal.valueOf(25000002500000.25);
        BigDecimal secondValue = BigDecimal.valueOf(75000007500000.75);
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(BigDecimal.valueOf(2.0));
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[12500001250000.125, 37500003750000.375]", result.toString());
    }

    @Test
    public void divisionBigInteger() {
        BigInteger firstValue = new BigInteger("25000002500000");
        BigInteger secondValue = new BigInteger("75000007500000");
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(new BigInteger("2"));
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[12500001250000, 37500003750000]", result.toString());
    }

    @Test
    public void divisionBigIntegerInteger() {
        BigInteger firstValue = new BigInteger("25000002500000");
        BigInteger secondValue = new BigInteger("75000007500000");
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.setModifier(2500000);
        List<Number> result = this.calculator.processCalculations();
        assertEquals("[10000001, 30000003]", result.toString());
    }

    @Test(expected = ArithmeticException.class)
    public void divisionThroughZeroException() {
        Integer value = 10;
        this.calculator.addValue(value);
        this.calculator.setModifier(0);
        this.calculator.processCalculations();
    }
}