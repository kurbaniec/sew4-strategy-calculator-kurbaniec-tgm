package strategy.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Kacper Urbaniec
 * @version 11.02.2019
 */
public class CalculatorTest {
    private Calculator<Number> calculator;

    @Before
    public void setUp() throws Exception {
        this.calculator = new Calculator<Number>();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void processCalculations() {
    }

    @Test
    public void setModifier() {
    }

    /**
     Vielleicht für die Zukunft
     try {
        this.calculator.addValue(42.0);
     }
     catch (Exception ex) {
        fail("Baum");
     }
     */
    @Test
    public void addProperDoubleValue() {
        Double value = 42.0;
        this.calculator.addValue(value);
        List<Double> list = new ArrayList<Double>();
        list.add(value);
        assertEquals(this.calculator.toString(), list.toString());

    }

    @Test
    public void addTwoProperDoubleValues() {
        Double firstValue = 42.0;
        Double secondValue = 23.0;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        List<Double> list = new ArrayList<Double>();
        list.add(firstValue);
        list.add(secondValue);
        assertEquals(this.calculator.toString(), list.toString());
    }

    @Test
    public void addTwoProperIntegerValues() {
        Integer firstValue = 42;
        Integer secondValue = 23;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        List<Integer> list = new ArrayList<>();
        list.add(firstValue);
        list.add(secondValue);
        assertEquals(this.calculator.toString(), list.toString());
    }

    @Test
    public void removeValue() {
        Integer firstValue = 42;
        Integer secondValue = 23;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        List<Integer> list = new ArrayList<>();
        list.add(firstValue);
        list.add(secondValue);
        list.remove(firstValue);
        this.calculator.removeValue(firstValue);
        assertEquals(this.calculator.toString(), list.toString());
    }

    @Test
    public void setCalculations() {
        try {
            this.calculator.setCalculations(new Addition());
            this.calculator.setCalculations(new Subtraction());
            this.calculator.setCalculations(new Multiplication());
            this.calculator.setCalculations(new Division());
        }
        catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void testtoString() {
        Double firstValue = 42.0;
        Double secondValue = 23.0;
        Integer thirdValue = 18;
        this.calculator.addValue(firstValue);
        this.calculator.addValue(secondValue);
        this.calculator.addValue(thirdValue);
        assertEquals(this.calculator.toString(), "[42.0, 23.0, 18]");
    }
}